from csv import reader
from json import dump

with open('fact_sessions.csv') as csvfile:
  csvreader = reader(csvfile, delimiter = ',')
  keys = csvreader.next()
  records = []

  for row in csvreader:
    record = dict()
    num_values = len(keys)
    for i in range(num_values):
      record[keys[i]] = row[i]
    records.append(record)

with open('fact_sessions.json','w') as jsonfile:
  for record in records:
    dump(record, jsonfile)
    jsonfile.write('\n')
