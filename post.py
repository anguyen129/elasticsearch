from os import system
from requests import get, head, delete, put, post
from pprint import pprint
from json import loads, dumps

# Target index
index = 'fact'
topic = 'sessions'
base = 'http://localhost:9200'
url = base + '/' + index

# Delete existing index for fresh start
r = delete(url)

# Get response
r = head(url)

# Check if index exists
if r.status_code == 200:
  print 'Index ' + index + ' exists'
else:
  print 'Index ' + index + ' does not exist'
  
  # Create index
  r = put(url)
  
  # Read data
  fname = 'fact_sessions.json'
  with open(fname) as infile:
    records = map(loads, infile.readlines())
  
  # Post data
  for i in range(len(records)):
    p = url + '/' + topic + '/' + str(i)
    d = dumps(records[i])
    print p
    print d
    r = post(p, data = d)
    print r.status_code

# Get state of existing indices
r = get(base + '/' + '_cat/indices?v')
print r.text

# Query for some data
r = get(url + '/' + '_search?q=*&pretty')
pprint(loads(r.text))

# r = get('http://localhost:9200/_cat/indices?v')

# print r

# r = delete('http://localhost:9200/fact')

# print dir(r)

# print r.status_code == True

# curl = '''curl -X DELETE 'http://localhost:9200/fact/\''''
# command = curl
# system(command)

# with open ('fact_sessions.json') as jsonfile:
#   records = jsonfile.readlines()

# entryid = 1

# for record in records:
#   curl = '''curl -X POST 'http://localhost:9200/fact/sessions/'''+str(entryid)+'\' -d '''
#   command = curl + '\'' + record.strip() + '\''
#   print command
#   system(command)
#   entryid += 1
